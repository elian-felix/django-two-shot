from django.urls import path
from receipts.views import create_account, create_category, create_receipt, list_accounts, list_categories, list_receipts


urlpatterns = [
    path('', list_receipts, name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('accounts/', list_accounts, name='account_list'),
    path('accounts/create/', create_account, name='create_account'),
    path('categories/', list_categories, name='category_list'),
    path('categories/create/', create_category, name='create_category'),
]
