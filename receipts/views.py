from django.shortcuts import redirect, render
from receipts.forms import CategoryForm, AccountForm, ReceiptForm
from receipts.models import *
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {'receipts': receipts}

    return render(request, 'receipts/index.html', context)


@login_required
def list_categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {'categories': categories}

    return render(request, 'receipts/list_categories.html', context)


@login_required
def list_accounts(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {'accounts': accounts}

    return render(request, 'receipts/list_accounts.html', context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        receipt_form = ReceiptForm(request.POST)
        if receipt_form.is_valid():
            new_receipt = receipt_form.save(False)
            new_receipt.purchaser = request.user
            new_receipt.save()
            return redirect('home')
    else:
        receipt_form = ReceiptForm()

    context = { 'receipt_form': receipt_form}

    return render(request, 'receipts/create_receipt.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        account_form = AccountForm(request.POST)
        if account_form.is_valid():
            new_account = account_form.save(commit=False)
            new_account.owner = request.user
            new_account.save()
            return redirect('account_list')
    else:
        account_form = AccountForm()

    context = { 'account_form': account_form}

    return render(request, 'receipts/create_account.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        category_form = CategoryForm(request.POST)
        if category_form.is_valid():
            new_category = category_form.save(commit=False)
            new_category.owner = request.user
            new_category.save()
            return redirect('category_list')
    else:
        category_form = CategoryForm()

    context = { 'category_form': category_form}

    return render(request, 'receipts/create_category.html', context)