from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ['name', 'id']


@admin.register(Account)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ['name', 'owner', 'id']


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ['vendor', 'purchaser','id']